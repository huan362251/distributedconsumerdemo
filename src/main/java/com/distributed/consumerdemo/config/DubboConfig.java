package com.distributed.consumerdemo.config;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.stereotype.Component;

@EnableDubbo(scanBasePackages = "com.distributed.consumerdemo")
@Component
public class DubboConfig {


}
