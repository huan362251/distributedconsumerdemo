package com.distributed.consumerdemo.kafka.consumer;

import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@RestController
@RequestMapping("/kafkaComsu")
public class KafkaComsu {

    /**
     * 最简单发送消息方式
     */
    @PostMapping("/createConsumer")
    public void createConsumer(){
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers","192.168.237.150:9092,192.168.237.151:9092,192.168.237.152:9092");
        //消费者群组定义
        kafkaProps.put("group.id","CountryCounter");
        kafkaProps.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        kafkaProps.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(kafkaProps);
        consumer.subscribe(Collections.singletonList("CustomerCountry"));
        //方法过时了，看着应该是切换为seek方法，暂时不使用了
        try{
            while (true) {
                Map<String,Integer> custCountryMap = new HashMap<>();
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String,String> record : records){
                    int updatedCount = 1;
                    if(custCountryMap.containsValue(record.value())){
                        updatedCount = custCountryMap.get(record.value()) + 1;
                    }
                    custCountryMap.put(record.value(),updatedCount);
                    Object json = JSONObject.toJSON(custCountryMap);
                    System.out.println(json.toString());
                }
            }
        }finally {
            consumer.close();
        }



    }
}
