package com.distributed.consumerdemo.kafka.product;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/kafkaProd")
public class KafkaProd {

    /**
     * 最简单发送消息方式
     */
    @PostMapping("/createProduce")
    public void createProduce(){
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers","192.168.237.150:9092,192.168.237.151:9092,192.168.237.152:9092");
        kafkaProps.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer producer = new KafkaProducer<String, String>(kafkaProps);
        ProducerRecord<String, String> record = new ProducerRecord<>("CustomerCountry", "Precision Products", "France");
        producer.send(record);
    }

    /**
     * 保护线程发送消息
     */
    @PostMapping("/createSyncProduce")
    public void createSyncProduce(){
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers","192.168.237.150:9092,192.168.237.151:9092,192.168.237.152:9092");
        kafkaProps.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer producer = new KafkaProducer<String, String>(kafkaProps);
        ProducerRecord<String, String> record = new ProducerRecord<>("CustomerCountry", "Precision Products", "France");
        try {
            RecordMetadata recordMetadata = (RecordMetadata) producer.send(record).get();
            System.out.println(recordMetadata.offset());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    /**
     * 保护线程发送消息
     */
    @PostMapping("/createAsyncProduce")
    public void createAsyncProduce(){
        Properties kafkaProps = new Properties();
        //特意配置的错误broker连接地址，为了显示异常
//        kafkaProps.put("bootstrap.servers","193.168.237.150:9092,193.168.237.151:9092,193.168.237.152:9092");
        kafkaProps.put("bootstrap.servers","192.168.237.150:9092,192.168.237.151:9092,192.168.237.152:9092");
        kafkaProps.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer producer = new KafkaProducer<String, String>(kafkaProps);
        ProducerRecord<String, String> record = new ProducerRecord<>("CustomerCountry", "Precision Products", "France");
        producer.send(record,new DemoProdCallback());
    }

    private class DemoProdCallback implements Callback{
        @Override
        public void onCompletion(RecordMetadata metadata, Exception exception) {
            if(exception != null){
                exception.printStackTrace();
            }else {
                System.out.println("metadata = " + metadata.offset());
            }
        }
    }

    public void createPartition(){
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers","192.168.237.150:9092,192.168.237.151:9092,192.168.237.152:9092");
        kafkaProps.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer producer = new KafkaProducer<String, String>(kafkaProps);
        //不输入key的情况下会按null来处理，会使用轮询算法分布在分区上
        ProducerRecord<String, String> record = new ProducerRecord<>("CustomerCountry", "France");
        producer.send(record);
    }
}
