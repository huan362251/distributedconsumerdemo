package com.distributed.consumerdemo.kafka.product;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.InvalidRecordException;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;

import java.util.List;
import java.util.Map;

public class BananaPartitioner implements Partitioner {

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        //获取topic的分区
        List<PartitionInfo> partitionInfos = cluster.partitionsForTopic(topic);
        //获取topic分区的长度
        int numPartitions = partitionInfos.size();
        if(keyBytes == null || (!(key instanceof String))){
            throw new InvalidRecordException("We expect all messages to have customer name as key");
        }
        //Banana分在最后一个分区上
        if(((String) key).equalsIgnoreCase("Banana")){
            return numPartitions;
        }
        //其它的分布在前面的分区上
        return (Math.abs(Utils.murmur2(keyBytes)) & (numPartitions -1));

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
