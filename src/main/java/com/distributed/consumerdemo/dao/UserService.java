package com.distributed.consumerdemo.dao;

import user.UserDTO;

public interface UserService {

    UserDTO queryUser(String id);

    void saveUser(UserDTO userDTO);

}
