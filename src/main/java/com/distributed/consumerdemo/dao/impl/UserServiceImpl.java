package com.distributed.consumerdemo.dao.impl;

import com.distributed.consumerdemo.dao.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import user.UserDTO;

@Service
public class UserServiceImpl implements UserService {

    @Reference
    private user.UserService userService;

    @Override
    public UserDTO queryUser(String id) {
        return userService.queryUser(id);
    }

    @Override
    public void saveUser(UserDTO userDTO) {
        userService.saveUser(userDTO);
    }
}
