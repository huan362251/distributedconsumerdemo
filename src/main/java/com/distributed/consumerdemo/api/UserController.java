package com.distributed.consumerdemo.api;

import com.distributed.consumerdemo.entity.common.QueryUserReqDTO;
import com.distributed.consumerdemo.dao.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import user.UserDTO;


@RestController
@RequestMapping("/user")
@Api(value = "user用户API")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/queryUser")
    @ApiOperation(value = "queryUser", httpMethod = "POST",notes = "查询用户信息")
    public void queryUser(QueryUserReqDTO userReqDTO){
        userService.queryUser(userReqDTO.getId());
    }

    @PostMapping("/saveUser")
    @ApiOperation(value = "saveUser", httpMethod = "POST",notes = "保存用户信息")
    public void saveUser(UserDTO userDTO){
        userService.saveUser(userDTO);
    }

}
