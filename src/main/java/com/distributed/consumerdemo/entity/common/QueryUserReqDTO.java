package com.distributed.consumerdemo.entity.common;

import lombok.Data;

@Data
public class QueryUserReqDTO {

    private String id;
}
